** Declaration **

Name: Conor O'Neill
Student Number: 10381171
Module: Compiler Construction
Stream: CASE4

I declare that this material, which I now submit for assessment, is entirely my own work and has not been taken from the work of others, save and to the extent that such work has been cited and acknowledged within the text of my work.  I understand that plagiarism, collusion, and copying are grave and serious offences in the university and accept the penalties that would be imposed should I engage in plagiarism, collusion or copying.  I have read and understood the Assignment Regulations set out in the module documentation.  I have identified and included the source of all facts, ideas, opinions, and viewpoints of others in the assignment references.  Direct quotations from books, journal articles, internet sources, module text, or any other source whatsoever are acknowledged and the source cited are identified in the assignment references.  This assignment, or any part of it, has not been previously submitted by me or any other person for assessment on this or any other course of study. I have read and understood the referencing guidelines found at http://www.library.dcu.ie/citing&refguide08.pdf and/or recommended in the assignment guidelines. 

Conor O'Neill. (3/11/15)

** Description **
Section one contains Tokenizer options to be set in Javacc, since BasicL is not case sensitive I have set IGNORE_CASE to true.

Section two contains driver code.

I declare an object of type BasicLParser, and instantiate it to take input from standard input(SI) or from a file depending on how the file is run. (Eg. java BasicLParser = SI | java BasicLParser filename.bl = read from file). Next there is some proper usage error messages.
I then iterate through the tokens consumed and print out their type and value(t.image).

Section 3 contains Token / Skip Definitions

First I instruct the lexer to skip single/multi line comments.
Next I ignore spaces, tabs, newlines, form feeds & carriage returns.
The keywords tokens are next specified along with the identifier token(letter followed by zero or more letters, digits or underscores).
#Tokens are private tokens which are only used to aid in the production of "proper" tokens.
Following this I specify a decimal number, operators / relations and a "catch anything else" other token.

Section 4 contains grammar production rules.
Most of this section is a translation of the grammar specified on your website into javacc syntax. However, there are two things here which should be explained.
1. My use of LOOKAHEAD(X), this is used if there is ambiguity as to which path a production rule should take.
(Eg. example -> <id> | <id> <bracket> ......) by using lookahead to see what follows id we can decide which rule to follow.
2. My elimination of left recursion by using the formula described in Top-Down-Parsing notes. I did this by following the formula: 
A := A alpha | beta
A := beta APrime
APrime := alpha APrime | Epsilon.

My use of this is evident in condition / fragment rules at the bottom of my file.

Conor.  


